package bankingaccounts;

public enum AccountType {
	CHECKING("Checking Account"), JOINT_CHECKING("Joint Checking Account");
	
	String title;
	
	AccountType(String title) {
		this.title = title;
	}
	
	@Override
	public String toString() {
		return title;
	}
}
