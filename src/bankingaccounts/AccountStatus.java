package bankingaccounts;

public enum AccountStatus {
	ACTIVE("Active"), CLOSED("Closed"), APPLIED("Awaiting Review"), REJECTED("Rejected");
	
	String text;
	
	AccountStatus(String text) {
		this.text = text;
	}
	
	@Override
	public String toString() {
		return text;
	}
}
