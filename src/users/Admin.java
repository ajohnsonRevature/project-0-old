package users;

class Admin extends Employee {
	
	Admin(String username, String password, String ssn) {
		super(username, password, ssn, UserType.ADMIN);
	}
}
