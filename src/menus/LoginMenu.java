package menus;

import exceptions.*;
import users.Employee;
import users.User;

class LoginMenu extends Menu {

	LoginMenu() {
		super(MenuType.LOGIN);
	}
	
	@Override
	public void populateOptions() {
		//no menu options for login
	}

	@Override
	public MenuReturn doMenuAction(int selection) {
		try {
			User U = loginExistingUser();
			if(U == null) {
				System.out.println("Something went wrong logging in...");
				return MenuReturn.GOHOME;
			}
			System.out.println("Login successful!");
			Menu M = null;
			switch(U.getUserType()) {
			case CUSTOMER:
				M = new CustomerMainMenu(U);
				break;
			case EMPLOYEE:
			case ADMIN:
				M = new EmployeeAdminMainMenu((Employee) U);
				break;
			}
			return M.doAbstractMenuAction();
		} catch (EmptyCancellationException e) {
			System.out.println(e.getMessage());
		}
		return MenuReturn.GOHOME;
	}
	
	private User loginExistingUser() throws EmptyCancellationException {
		User U = null;
		String username = "";
		boolean existingUsername = false;
		boolean correctPassword = false;
		while(!existingUsername) {
			System.out.println("Please enter your username, or enter blank to cancel.");
			try {
				username = getExistingUsername();
				U = User.getUserByUsername(username);
				existingUsername = true;
			} catch (NonexistingUsernameException e) {
				System.out.println(e.getMessage());
			}
		}
		while(!correctPassword) {
			System.out.println("Please enter the password for username: " + username + " or enter blank to cancel.");
			if(verifyCorrectPassword(U)) {
				correctPassword = true;
			} else {
				System.out.println("Incorrect password!");
			}
		}
		return U;
	}
	
	private boolean verifyCorrectPassword(User U) throws EmptyCancellationException {
		String candidatePassword = MenuMasterControl.getUserInputNonempty();
		return U.checkPassword(candidatePassword);
	}
	
	private String getExistingUsername() throws NonexistingUsernameException, EmptyCancellationException {
		String username = MenuMasterControl.getUserInputNonempty();
		if(!User.usernameAlreadyExists(username)) {
			throw new NonexistingUsernameException();
		}
		return username;
	}

	@Override
	public void generateTitles() {
		menuTitle = new StringBuffer("Existing User Login");
		menuSubTitle = new StringBuffer("");
	}
}
