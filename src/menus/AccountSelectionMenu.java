package menus;

import java.util.ArrayList;
import java.util.List;

import bankingaccounts.Account;
import bankingaccounts.AccountStatus;
import users.Customer;
import users.Employee;
import users.User;
import users.UserType;

class AccountSelectionMenu extends Menu {

	private boolean fromCustomer = false;
	private Customer sourceCustomer = null;

	private User loggedInUser;
	private UserType UType;

	List<Account> accessibleAccounts;

	AccountSelectionMenu(User loggedInUser) {
		super(MenuType.ACCOUNT_SELECTION);
		this.loggedInUser = loggedInUser;
		UType = loggedInUser.getUserType();
	}

	AccountSelectionMenu(Employee loggedInEmployee, Customer sourceCustomer) {
		// overloaded constructor for the case when this menu is accessed from the
		// customer selection menu
		// in this case, an admin can transfer money to any other account, not just the
		// customer's
		this(loggedInEmployee);
		fromCustomer = true;
		this.sourceCustomer = sourceCustomer;
	}

	@Override
	public void generateTitles() {
		menuTitle = new StringBuffer("Banking Account Selection Menu");
		if(UType != UserType.CUSTOMER) {
			if (fromCustomer) {
				menuTitle.append("\n(Accounts Owned By " + sourceCustomer.getDisplayString() + ")");
			} else {
				menuTitle.append("\n(All Banking Accounts)");
			}
		}
		menuSubTitle = new StringBuffer("");
		if (fromCustomer) {
			menuSubTitle.append("Select an active account to ");
		} else {
			menuSubTitle.append("Select an account to ");
		}
		switch (UType) {
		default:
		case CUSTOMER:
			menuSubTitle.append("to manage (withdraw from, deposit to, or transfer from).");
			break;
		case EMPLOYEE:
			menuSubTitle.append("view.");
			break;
		case ADMIN:
			if (fromCustomer) {
				menuSubTitle.append("manage (withdraw from, deposit to, transfer from, or close if active).");
			} else {
				menuSubTitle.append(
						"manage (withdraw from, deposit to, transfer from, close if active, restore if closed, or approve if rejected).");
			}
			break;
		}
		menuSubTitle.append("\n");
	}

	@Override
	public MenuReturn doMenuAction(int selection) {
		switch (selection) {
		case 0:
			return MenuReturn.EXIT;
		case 1:
			return MenuReturn.BACK;
		default:
			Account selectedAccount = accessibleAccounts.get(selection - 2);
			List<Account> nextAccessibleAccounts;
			if (fromCustomer) {
				nextAccessibleAccounts = Account.getAllAccountsOfStatus(AccountStatus.ACTIVE, AccountStatus.CLOSED);
			} else {
				nextAccessibleAccounts = accessibleAccounts;
			}
			Menu M = new AccountManagementMenu(loggedInUser, selectedAccount, nextAccessibleAccounts);
			return M.doAbstractMenuAction();
		}
	}

	@Override
	public void populateOptions() {
		accessibleAccounts = new ArrayList<>();
		addOption("Go Back");
		switch (UType) {
		default:
		case CUSTOMER:
			accessibleAccounts = Account.getCustomersActiveAccounts((Customer) loggedInUser);
			break;
		case EMPLOYEE:
		case ADMIN:
			if (fromCustomer) {
				accessibleAccounts = Account.getCustomersAccountsOfStatus(sourceCustomer, AccountStatus.ACTIVE);
			} else {
				accessibleAccounts = Account.getAllAccountsOfStatus(AccountStatus.ACTIVE, AccountStatus.CLOSED,
						AccountStatus.REJECTED);
			}
			break;
		}
		for (Account A : accessibleAccounts) {
			addOption("Account: " + A.getDisplayString());
		}
	}
}
