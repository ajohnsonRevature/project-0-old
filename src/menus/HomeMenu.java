package menus;

class HomeMenu extends Menu {
	
	HomeMenu() {
		super(MenuType.HOME);
	}

	@Override
	public MenuReturn doMenuAction(int selection) {
		Menu M;
		switch(selection) {
		case 0:
			return MenuReturn.EXIT;
		case 1:
			M = new NewUserMenu();
			return M.doAbstractMenuAction();
		case 2:
			M = new LoginMenu();
			return M.doAbstractMenuAction();
		default:
			return MenuReturn.ERROR;
		}
	}

	@Override
	public void populateOptions() {
		addOption("Register New User");
		addOption("Login Existing User");
	}

	@Override
	public void generateTitles() {
		menuTitle = new StringBuffer("Banking Application Home");
		menuSubTitle = new StringBuffer("");
	}

}
