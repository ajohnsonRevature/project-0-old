package exceptions;

public class ExistingUsernameException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public ExistingUsernameException() {
		super("Username already exists.");
	}

}
