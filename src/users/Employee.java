package users;

public class Employee extends User{
	
	public Employee(String username, String password, String ssn) {
		super(username, password, ssn, UserType.EMPLOYEE);
	}
	
	public Employee(String username, String password, String ssn, UserType UType) {
		super(username, password, ssn, UType);
	}
}
