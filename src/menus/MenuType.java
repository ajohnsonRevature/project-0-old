package menus;

enum MenuType {
	HOME(true), LOGIN(false), NEWUSER(true), REGISTER(false), UNTYPED(false),
	CUSTOMER_MAIN(true), ACCOUNT_SELECTION(true), ACCOUNT_APPLY(true),
	ACCOUNT_APPLICATION_ACCEPTDENY(true), ACCOUNT_MANAGEMENT(true), ACCOUNT_APPLICATION_SELECTION(true), EMPLOYEE_SELECT_CUSTOMER(true),
	TRANSFER(true);
	
	boolean optioned;
	
	MenuType(boolean optioned) {
		this.optioned = optioned;
	}
}
