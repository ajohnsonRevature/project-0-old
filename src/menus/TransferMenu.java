package menus;

import java.util.ArrayList;
import java.util.List;

import bankingaccounts.Account;
import exceptions.EmptyCancellationException;
import exceptions.InsufficientBalanceException;
import exceptions.MissingAccountException;
import exceptions.NonpositiveException;

import bankingaccounts.AccountStatus;

class TransferMenu extends Menu {

	private List<Account> targetAccounts;
	private Account sourceAccount;

	TransferMenu(Account sourceAccount, List<Account> targetAccounts) {
		super(MenuType.TRANSFER);
		this.sourceAccount = sourceAccount;
		this.targetAccounts = targetAccounts;
	}

	@Override
	public MenuReturn doMenuAction(int selection) {
		switch (selection) {
		case 0:
			return MenuReturn.EXIT;
		case 1:
			return MenuReturn.BACK;
		default:
			try {
				doTransfer(targetAccounts.get(selection - 2));
				System.out.println("Transfer completed.");
			} catch (EmptyCancellationException e) {
				System.out.println(e.getMessage());
				return MenuReturn.STAY;
			}
			return MenuReturn.BACK;
		}
	}

	private void doTransfer(Account targetAccount) throws EmptyCancellationException {
		boolean goodTransfer = false;
		while (!goodTransfer) {
			System.out.println("Enter the amount you would like to transfer.");
			double transferAmount = MenuMasterControl.getUserInputDouble();
			try {
				Account.doTransfer(transferAmount, sourceAccount, targetAccount);
				goodTransfer = true;
			} catch (NonpositiveException | InsufficientBalanceException e) {
				System.out.println(e.getMessage());
			}
		}
	}

	@Override
	public void populateOptions() {
		addOption("Go Back");
		for (Account acc : targetAccounts) {
			addOption("Account: " + acc.getDisplayString());
		}
	}

	@Override
	public void generateTitles() {
		menuTitle = new StringBuffer("Balance Transfer Menu");
		menuSubTitle = new StringBuffer("Select a Target Account to Initiate Transfer from Source Account "
				+ sourceAccount.getDisplayString() + "\n");
	}
}
