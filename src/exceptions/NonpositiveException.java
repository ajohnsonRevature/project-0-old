package exceptions;

public class NonpositiveException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Override
	public String getMessage() {
		return "You must deposit a positive amount of money!";
	}

}
