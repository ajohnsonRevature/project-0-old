package bankingaccounts;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import exceptions.DepositTooBigException;
import exceptions.EmptyCancellationException;
import exceptions.InsufficientBalanceException;
import exceptions.MissingAccountException;
import exceptions.NonpositiveException;
import exceptions.WithdrawTooBigException;
import users.Customer;
import users.User;
import users.UserType;

public class Account implements Serializable {

	private static Map<AccountID, Account> allBankingAccounts = new HashMap<AccountID, Account>();
	private static final double MAX_DAILY_WITHDRAW = 10000;
	private static final int NUM_DIGITS_TO_DISPLAY = 2;
	private static final double MAX_DEPOSIT = 10000;

	private AccountID aid;

	private Customer primaryOwner;
	private Customer secondaryOwner;

	private double balance = 0;
	private AccountStatus AStatus;
	private AccountType AType;

	public Account(Customer primaryOwner, AccountStatus AStatus, AccountType AType, AccountID aid) {
		this.primaryOwner = primaryOwner;
		this.AStatus = AStatus;
		this.AType = AType;
		this.aid = aid;
	}

	public Account(Customer primaryOwner, Customer secondaryOwner, AccountStatus AStatus, AccountType AType,
			AccountID aid) {
		this(primaryOwner, AStatus, AType, aid);
		this.secondaryOwner = secondaryOwner;
	}

	@Override
	public boolean equals(Object o) {
		return (o instanceof Account) && ((Account) o).aid.equals(aid);
	}

	public String getApplicationDisplayString() {
		String display = "";
		display = "[Account ID = " + aid.toString() + "][Account Type = " + AType.toString() + "][Applicant = "
				+ primaryOwner.getDisplayString() + "]";
		if (AType == AccountType.JOINT_CHECKING) {
			display += "[Proposed Second Owner = " + secondaryOwner.getDisplayString() + "]";
		}
		display += "[Application Status = " + AStatus.toString() + "]";
		return display;
	}

	private static AccountID getAvailableAccountID() {
		// a better method can be implemented here eventually
		int firstUnused = 0;
		while (allBankingAccounts.get(AccountID.getIDFromInt(firstUnused)) != null) {
			firstUnused++;
		}
		return new AccountID(firstUnused);
	}

	public static AccountID createNewAccount(Customer primaryOwner, AccountStatus AStatus, AccountType AType) {
		AccountID aid = getAvailableAccountID();
		Account A = new Account(primaryOwner, AStatus, AType, aid);
		allBankingAccounts.put(aid, A);
		return aid;
	}

	public static AccountID createNewAccount(Customer primaryOwner, Customer secondaryOwner, AccountStatus AStatus,
			AccountType AType) {
		AccountID aid = getAvailableAccountID();
		Account A = new Account(primaryOwner, secondaryOwner, AStatus, AType, aid);
		allBankingAccounts.put(aid, A);
		return aid;
	}

	public static void setAllAccounts(Map<AccountID, Account> inAllBankingAccounts) {
		allBankingAccounts = inAllBankingAccounts;
	}

	public static Map<AccountID, Account> getAllAccountsMap() {
		return allBankingAccounts;
	}

	@Deprecated
	public static List<Account> getUsersApplications(User U) {
		List<Account> openApplications = new ArrayList<>();
		for (Account A : allBankingAccounts.values()) {
			if ((A.AStatus == AccountStatus.APPLIED || A.AStatus == AccountStatus.REJECTED)
					&& A.getPrimaryOwner().equals(U)) {
				openApplications.add(A);
			}
		}
		return openApplications;
	}

	private User getPrimaryOwner() {
		return primaryOwner;
	}

	public void setStatus(AccountStatus AStatus) {
		this.AStatus = AStatus;
	}

	public static List<Account> getCustomersActiveAccounts(Customer loggedInUser) {
		List<Account> usersActiveAccounts = new ArrayList<>();
		for (AccountID aid : loggedInUser.getAIDs()) {
			Account acc = allBankingAccounts.get(aid);
			if (acc.getStatus() == AccountStatus.ACTIVE) {
				usersActiveAccounts.add(acc);
			}
		}
		return usersActiveAccounts;
	}

	public String getDisplayString() {
		String display = "";
		display = "[Account ID = " + aid.toString() + "][Account Type = " + AType.toString() + "][Primary Owner = "
				+ primaryOwner.getDisplayString() + "]";
		if (AType == AccountType.JOINT_CHECKING) {
			display += "[Secondary Owner = " + secondaryOwner.getDisplayString() + "]";
		}
		display += "[Balance = " + getBalanceString() + "]" + "[Status = " + AStatus.toString() + "]";
		return display;
	}

	public static double roundToDigits(double x, int digits) {
		return Math.round(x * Math.pow(10, digits)) / Math.pow(10, digits);
	}

	public String getBalanceString() {
		return Double.toString(roundToDigits(balance, NUM_DIGITS_TO_DISPLAY));
	}

	public AccountStatus getStatus() {
		return AStatus;
	}

	public static List<Account> getAllAccountsOfStatus(AccountStatus status1) {
		return getAllAccountsOfStatus(status1, status1);
	}

	public static List<Account> getAllAccountsOfStatus(AccountStatus status1, AccountStatus status2) {
		return getAllAccountsOfStatus(status1, status1, status2);
	}

	public static List<Account> getAllAccountsOfStatus(AccountStatus status1, AccountStatus status2,
			AccountStatus status3) {
		List<Account> validAccounts = new ArrayList<>();
		for (Account A : allBankingAccounts.values()) {
			if (A.AStatus == status1 || A.AStatus == status2 || A.AStatus == status3) {
				validAccounts.add(A);
			}
		}
		return validAccounts;
	}

	public void makeWithdraw(double withdraw, Customer customerWithdrawee)
			throws WithdrawTooBigException, NonpositiveException, InsufficientBalanceException {
		if (withdraw <= 0) {
			throw new NonpositiveException();
		}
		if (withdraw > balance) {
			throw new InsufficientBalanceException();
		}
		if (customerWithdrawee != null && withdraw + customerWithdrawee.getAmountWithdrawnToday() > MAX_DAILY_WITHDRAW) {
			throw new WithdrawTooBigException();
		}
		makeWithdrawNocheck(withdraw, customerWithdrawee);
	}

	public void makeDeposit(double deposit) throws DepositTooBigException, NonpositiveException {
		if (deposit > getMaxDeposit()) {
			throw new DepositTooBigException();
		}
		if (deposit <= 0) {
			throw new NonpositiveException();
		}
		makeDepositNocheck(deposit);
	}

	private void makeWithdrawNocheck(double withdraw, Customer customerWithdrawee) {
		if (customerWithdrawee != null) {
			customerWithdrawee.addToWithdrawToday(withdraw);
		}
		balance -= withdraw;
	}

	private void makeDepositNocheck(double deposit) {
		balance += deposit;
	}

	public static List<Account> getCustomersAccountsOfStatus(Customer sourceCustomer, AccountStatus status1) {
		return getCustomersAccountsOfStatus(sourceCustomer, status1, status1);
	}

	public static List<Account> getCustomersAccountsOfStatus(Customer sourceCustomer, AccountStatus status1,
			AccountStatus status2) {
		return getCustomersAccountsOfStatus(sourceCustomer, status1, status1, status2);
	}

	public static List<Account> getCustomersAccountsOfStatus(Customer sourceCustomer, AccountStatus status1,
			AccountStatus status2, AccountStatus status3) {
		List<Account> validAccounts = new ArrayList<>();
		for (AccountID aid : sourceCustomer.getAIDs()) {
			Account A = allBankingAccounts.get(aid);
			if (A == null) {
				throw new MissingAccountException();
			}
			if (A.AStatus == status1 || A.AStatus == status2 || A.AStatus == status3) {
				validAccounts.add(A);
			}
		}
		return validAccounts;
	}

	public double getBalance() {
		return balance;
	}

	public static void doTransfer(double amount, Account source, Account target)
			throws NonpositiveException, InsufficientBalanceException {
		if (amount <= 0) {
			throw new NonpositiveException();
		}
		if (source.getBalance() < amount) {
			throw new InsufficientBalanceException();
		}
		source.makeWithdrawNocheck(amount, null);
		target.makeDepositNocheck(amount);
	}

	public static double getMaxDeposit() {
		return MAX_DEPOSIT;
	}
}
