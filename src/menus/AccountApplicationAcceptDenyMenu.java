package menus;

import bankingaccounts.Account;
import bankingaccounts.AccountStatus;

class AccountApplicationAcceptDenyMenu extends Menu {
	
	Account selectedAccount;
	
	AccountApplicationAcceptDenyMenu(Account selectedAccount) {
		super(MenuType.ACCOUNT_APPLICATION_ACCEPTDENY);
		this.selectedAccount = selectedAccount;
	}

	@Override
	public void populateOptions() {
		addOption("Select a Different Application");
		addOption("Approve");
		addOption("Deny");
	}

	@Override
	public void generateTitles() {
		menuTitle = new StringBuffer("Application: " + selectedAccount.getApplicationDisplayString());
		menuSubTitle = new StringBuffer("Do you approve or deny this application?\n");
	}

	@Override
	public MenuReturn doMenuAction(int selection) {
		switch(selection) {
		case 0:
			return MenuReturn.EXIT;
		default:
		case 1:
			return MenuReturn.BACK;
		case 2:
			selectedAccount.setStatus(AccountStatus.ACTIVE);
			System.out.println("Application accepted.");
			return MenuReturn.BACK;
		case 3:
			selectedAccount.setStatus(AccountStatus.REJECTED);
			System.out.println("Application rejected.");
			return MenuReturn.BACK;
		} 
	}

}
