package users;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import bankingaccounts.AccountID;

public class Customer extends User implements Serializable {
	
	private List<AccountID> accountIDs = new ArrayList<>();
	private double amountWithdrawnToday;
	
	public Customer(String username, String password, String ssn) {
		super(username, password, ssn, UserType.CUSTOMER);
	}
	
	public void addAccountID(AccountID aid) {
		accountIDs.add(aid);
	}
	
	public List<AccountID> getAIDs() {
		return accountIDs;
	}
	
	public String getFullCustomerDisplayString() {
		return "[username = " + getUsername() + ", password = " + getPassword() + ", ssn = " + getSSN() + "]";
	}

	public double getAmountWithdrawnToday() {
		return amountWithdrawnToday;
	}

	public void setAmountWithdrawnToday(double amountWithdrawnToday) {
		this.amountWithdrawnToday = amountWithdrawnToday;
	}

	public void addToWithdrawToday(double withdraw) {
		amountWithdrawnToday += withdraw;
	}
}
